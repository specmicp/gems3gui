
TEMPLATE	= app
#LANGUAGE        = C++
TARGET		= gems3
#VERSION         = 3.9.2

DEFINES         += NODEARRAYLEVEL
#DEFINES         += NOMUPNONLOGTERM
DEFINES  += NO_JSONIO
#DEFINES += USE_NLOHMANNJSON
DEFINES += NDEBUG
#!win32:!macx-clang:DEFINES += OVERFLOW_EXCEPT  #compile with nan inf exceptions

CONFIG+=sdk_no_version_check
CONFIG += c++17
CONFIG += warn_on
#CONFIG += warn_off
#CONFIG += help
CONFIG += thread
QT += network
QT += sql
QT += xml
QT += svg
QT += charts
#Qt += printsupport
lessThan( QT_MAJOR_VERSION, 5 ): CONFIG += help
greaterThan( QT_MAJOR_VERSION, 4 ): QT += widgets printsupport help concurrent

#RESOURCES += img.qrc
win32:RC_ICONS += Gems3.ico

!win32 {
  DEFINES += __unix
  QMAKE_CFLAGS += pedantic -Wall -Wextra -Wwrite-strings -Werror


  QMAKE_CXXFLAGS += -Wall -Wextra -Wcast-align -Wpointer-arith \
   -Wmissing-declarations -Wundef \ #-Weffc++ -Wshadow -Wformat-nonliteral -Winline
   -Wcast-qual -Wwrite-strings -Wno-unused-parameter \
   -Wfloat-equal -pedantic -ansi #-fsignaling-nans -ffinite-math-only

}

macx-g++ {
  DEFINES += __APPLE__
# macx:QMAKE_MAC_SDK = /Developer/SDKs/MacOSX10.6.sdk
  CONFIG -= warn_on
  CONFIG += warn_off
}

RESOURCES      = ./GUI/GUI.qrc

SERVICES4_CPP  =  ./GUI/Services4
DATAMAN_CPP    =  ./GUI/Dataman
DIALOGS4_CPP   =  ./GUI/Dialogs4
CHARTS_CPP     =  ./GUI/charts
MODULES_CPP    =  ./Modules
SUBMODS_CPP    =  ./Modules/Submods
NUMERICS_CPP   =  ./Modules/Numerics
GEMS3K_CPP     =  ../standalone/GEMS3K

SERVICES4_H  =  $$SERVICES4_CPP
DATAMAN_H    =  $$DATAMAN_CPP
DIALOGS4_H   =  $$DIALOGS4_CPP
CHARTS_H     =  $$CHARTS_CPP
MODULES_H    =  $$MODULES_CPP
SUBMODS_H    =  $$SUBMODS_CPP
NUMERICS_H   =  $$NUMERICS_CPP
GEMS3K_H     =  $$GEMS3K_CPP

DEPENDPATH   += $$SERVICES4_H
DEPENDPATH   += $$DATAMAN_H  
DEPENDPATH   += $$DIALOGS4_H 
DEPENDPATH   += $CHARTS_H
DEPENDPATH   += $$MODULES_H
DEPENDPATH   += $$SUBMODS_H   
DEPENDPATH   += $$NUMERICS_H 
DEPENDPATH   += $$GEMS3K_H   

INCLUDEPATH   += $$SERVICES4_H
INCLUDEPATH   += $$DATAMAN_H  
INCLUDEPATH   += $$DIALOGS4_H 
INCLUDEPATH   += $$CHARTS_H
INCLUDEPATH   += $$MODULES_H
INCLUDEPATH   += $$SUBMODS_H   
INCLUDEPATH   += $$NUMERICS_H 
INCLUDEPATH   += $$GEMS3K_H   


MOC_DIR = tmp
UI_DIR  = $$MOC_DIR
UI_SOURSEDIR  = $$MOC_DIR
UI_HEADERDIR  = $$MOC_DIR
OBJECTS_DIR       = obj

include($$DATAMAN_CPP/Dataman.pri)
include($$MODULES_CPP/Modules.pri)
include($$SUBMODS_CPP/Submods.pri)
include($$NUMERICS_CPP/Numerics.pri)
include($$SERVICES4_CPP/Services4.pri)
include($$DIALOGS4_CPP/Dialogs4.pri)
include($$CHARTS_CPP/charts.pri)
include($$GEMS3K_CPP/gems3k.pri)

#message("Defines: $$DEFINES")
