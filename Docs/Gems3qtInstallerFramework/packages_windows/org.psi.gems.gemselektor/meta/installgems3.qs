
function Component()
{
    // default constructor
}

Component.prototype.createOperations = function()
{
    // call default implementation to actually install GEMS!
    component.createOperations();

    if (systemInfo.productType === "windows") {
        component.addOperation("CreateShortcut", "@TargetDir@/Gems3-app/gems3.exe", 
	    "@StartMenuDir@/GEM-Selektor.lnk", "workingDirectory=@TargetDir@", 
	    "description=Start GEM-Selektor");
    }
}
