Before creating the installer, copy the gems3.exe file 
from the /buildMSVC64bit-Release/release folder into /Gems3-app in this folder.
Then start the command prompt terminal from this folder, cd to /Gems3-app, and 
execute the runwindeployqt.bat script. In case of success, the windeployqt.exe file
can be deleted to save space in the installer file to be distributed to users.   