rem Edit the name of installer file to be generated, Gems3.9.0-xxxxxxx.yyyyyyy-win64-qtinstall.exe
rem by replacing xxxxxxx with commit # of gems3gui and yyyyyyy with commit # of gems3k repositories.
rem Then generate the installer by opening a command-line terminal from this directory and executing the following command:
C:\Qt\Tools\QtInstallerFramework\4.1\bin\binarycreator.exe --offline-only -c config/config.xml -p packages Gems3.9.2-96323c0.8986188-win64-qt6install.exe

